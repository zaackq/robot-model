#include "umrob/robot_model.h"

#include <fmt/format.h>
#include <fmt/ostream.h>

#include <fstream>
#include <sstream>
#include <filesystem>

//! \brief Look for a file named 'model_name'.urdf in the
//! 'share/robot-model/models' path next to 'exe_path'
//!
//! \param exe_path current executable path (typically argv[0])
//! \param model_name name of the model to load
//! \return std::string model description
std::string load_urdf(std::string_view exe_path, std::string_view model_name);

int main(int argc, char* argv[]) {
    // Load the URDF model from a file
    const std::string urdf = load_urdf(argv[0], "nao_arms");

    // Construct a robot model with a given URDF description
    auto model = umrob::RobotModel(urdf);

    // Set joint positions. Names are used instead of indices because indicies
    // stop making sense when you have a tree-like structure (e.g humanoid
    // robot). But you can use model.jointIndex(name) and model.jointIndex(idx)
    // convert between joint names and joint indices
    model.jointPosition("LShoulderPitch") = 0;
    model.jointPosition("LShoulderRoll") = 0;
    model.jointPosition("LElbowYaw") = 0;
    model.jointPosition("LElbowRoll") = 0;
    model.jointPosition("LWristYaw") = 0;
    model.jointPosition("LHand") = 0;
    model.jointPosition("RShoulderPitch") = 0;
    model.jointPosition("RShoulderRoll") = 0;
    model.jointPosition("RElbowYaw") = 0;
    model.jointPosition("RElbowRoll") = 0;
    model.jointPosition("RWristYaw") = 0;
    model.jointPosition("RHand") = 0;

    // Get references to the grippers' poses and Jacobians
    auto& r_gripper_pose = model.linkPose("r_gripper");
    auto& r_gripper_jacobian = model.linkJacobian("r_gripper");
    auto& l_gripper_pose = model.linkPose("l_gripper");
    auto& l_gripper_jacobian = model.linkJacobian("l_gripper");

    // Take the current joint state and use it to update all the links' pose and
    // Jacobians (forward kinematics)
    model.update();

    // Print the result
    fmt::print("\n{:-^80}\n", " Initial pose ");
    fmt::print("r_gripper pose:\n{}\n\n", r_gripper_pose.matrix());
    fmt::print("r_gripper jacobian:\n{}\n\n", r_gripper_jacobian.matrix);
    fmt::print("l_gripper pose:\n{}\n\n", l_gripper_pose.matrix());
    fmt::print("l_gripper jacobian:\n{}\n\n", l_gripper_jacobian.matrix);

    // Move some joints
    model.jointPosition("LShoulderPitch") = 1;
    model.jointPosition("RShoulderRoll") = 1;

    // Update the model
    model.update();
    // Print the result
    fmt::print("\n{:-^80}\n", " Final pose ");
    fmt::print("r_gripper pose:\n{}\n\n", r_gripper_pose.matrix());
    fmt::print("r_gripper jacobian:\n{}\n\n", r_gripper_jacobian.matrix);
    fmt::print("l_gripper pose:\n{}\n\n", l_gripper_pose.matrix());
    fmt::print("l_gripper jacobian:\n{}\n\n", l_gripper_jacobian.matrix);
}

std::string load_urdf(std::string_view exe_path, std::string_view model_name) {
    const auto binary_path = std::filesystem::path(exe_path).parent_path();
    const auto model_path =
        binary_path / std::filesystem::path(fmt::format(
                          "share/robot-model/models/{}.urdf", model_name));
    std::ifstream model_file(model_path.generic_string());
    if (not model_file.is_open()) {
        fmt::print(stderr, "Failed to open the {} URDF file\n", model_name);
        std::exit(-1);
    }

    return std::string{(std::istreambuf_iterator<char>(model_file)),
                       std::istreambuf_iterator<char>()};
}